# Nantes-Roller

Plugin to make easy the Nantes-Roller declaration from wordpress's site.

## Structures

declaration : All files for the Nantes-Roller declaration_rando
awaken_nr : Theme used by wordpress on the website
forum : Docker-compose to launch PhpBB for local test

## Git Workflow

https://nvie.com/posts/a-successful-git-branching-model/

## Development environnement

Needs :

- docker
- docker-compose

```
cd docker
docker-compose up -d
```

Open localhost:8000
Create a user for the test
Create the groups in "Staff Members"

## Build

```
zip -r nantes-roller.zip nantes-roller
zip -r theme.zip awaken_nr
```

Upload the plugin in wordpres

### Simple Staff List

https://fr.wordpress.org/plugins/simple-staff-list
version: 2.2.0

The plugin "declaration" use this module to show the user by group
The groups needs for the module (slub with this name is mandatory) :

- declaration_rando: Used by default to send the declaration to all users from this group
- responsable-randonnee : In charge to lead the "randonée roller"
- responsable-signaleurs : In charge to lead the "signaleurs"
- responsable-staffeurs : In charge to lead the "staffeurs"
- responsable-secouristes : In charge to lead the "secouristes"
- conducteurs : In charge to drive the "betaillères"
- cyclistes : In charge to close the "cortège" with bikes

Next, we can create some user on each group. The informations below are mandatory :

- Name
- Email
- Phone (only for the responsable-randonnee group )
