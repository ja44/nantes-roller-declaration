<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Awaken_nr
 */

get_header(); ?>
<div class="row">
	<?php is_rtl() ? $rtl = 'awaken-rtl' : $rtl = ''; ?>
	<div class="col-xs-12 col-sm-12 col-md-8 <?php echo $rtl ?>">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		

			<div><h1 class="awt-title" style="width: 100%">Le B.A.-BA de la rando roller</h1></div>
			<div class="rando_detail" id="rando_detail">
				<ul>
					<li>La rando roller est organisée <strong>tous les jeudis de l'année</strong> (sauf jours fériés, du 3 au 24 août ou en cas de pluie)</li>
					<li>Elle est <strong>ouverte à tous</strong>, même les non-adhérents</li>
					<li><strong>Savoir freiner et tourner</strong> est indispensable pour participer à la randonnée</li>
					<li>Nantes-Roller <strong>n'organise pas de cours</strong> de rollers</li>
					<li>Nantes-Roller <strong>ne prête pas et ne loue pas</strong> de rollers</li>
				</ul>
			</div>
			
			<br />
			<div class="awt-container"><h1 class="awt-title">Prochaine randonnée</h1></div>
			<?PHP 

			$fposts = new WP_Query( array(
						'posts_per_page' => 1,
						'post_type' => 'randonnee'
					));

			while( $fposts->have_posts() ) : $fposts->the_post(); 
				the_title( sprintf( '<h1 class="genpost-entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' );
				
				$url_thumbnail_post = get_the_post_thumbnail_url(get_post()->ID, 'medium' );
				if(!isset($url_thumbnail_post) || $url_thumbnail_post == "") {
					$url_thumbnail_post = get_the_post_thumbnail_url(@get_post_meta(get_post()->ID, 'rando_parcours', true), 'medium' );
				}
				echo "<div class='image_prochaine_rando'><a href='".esc_url( get_permalink() )."'><img src='".$url_thumbnail_post."' style='max-width:200px;' /></a></div>";


				echo "<div id='rando_content'>";
				the_content("\nLire le détail de la randonnée...");
				echo "</div>";

			endwhile; 
					
			wp_reset_postdata();

			?>
			
			<p style="margin-top:40px;"><a href="<?php echo get_site_url(); ?>/randonnee/">Liste des précédentes randonnées</a></p>

			<div class="awt-container"><h1 class="awt-title">Actualités</h1></div>
			
			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ 
					$counter = 0;
					$max_posts = 6;
				?>
			
				<div class="row">
				<?php while ( have_posts() && $counter < $max_posts ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
					?>
				
				<?php 
					$counter++;
					if ($counter % 2 == 0) {
						echo '</div><div class="row">';
					} 
				?>
				<?php endwhile; ?>

				<div class="col-xs-12 col-sm-12 col-md-12">
					<?php awaken_paging_nav(); ?>
				</div>
			</div><!-- .row -->

			<?php else : ?>

				<?php get_template_part( 'content', 'none' ); ?>

			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .bootstrap cols -->
<div class="col-xs-12 col-sm-6 col-md-4">
	<?php get_sidebar(); ?>
</div><!-- .bootstrap cols -->
</div><!-- .row -->
<?php get_footer(); ?>
