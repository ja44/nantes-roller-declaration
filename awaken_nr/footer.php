		</div><!-- container -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="footer-widget-area">
					<div class="col-md-9">
						<div class="left-footer">
							<div class="widget-area" role="complementary">
									
									<div class="column partners widget widget_text">
										<h1 class="footer-widget-title">Nos partenaires</h1>
										<ul>
											<?php if ( ! dynamic_sidebar( 'footer-left' ) ) : ?>

											<?php endif; // end sidebar widget area ?>
										</ul>
									</div>
								</aside>
							</div><!-- .widget-area -->
						</div>
					</div>

					<div class="col-md-3">
						<div class="right-footer">
							<div class="widget-area" role="complementary">
								<?php if ( ! dynamic_sidebar( 'footer-right' ) ) : ?>

								<?php endif; // end sidebar widget area ?>
							</div>
						</div>
					</div>						
					
				</div><!-- .footer-widget-area -->
			</div><!-- .row -->
		</div><!-- .container -->	

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>


<!--
<script type="text/javascript"> 
var pkBaseURL = (("https:" == document.location.protocol) ? "https://www.nantes-roller.com/piwik/" : "http://www.nantes-roller.com/piwik/"); 
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E")); 
</script>
<script type="text/javascript">
 try { 
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 1); 
piwikTracker.trackPageView(); piwikTracker.enableLinkTracking(); 
} catch( err ) {} 
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); 
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); 
</script> 
<script type="text/javascript"> var pageTracker = _gat._getTracker("UA-6022809-1"); pageTracker._trackPageview(); 
</script>
-->

<!-- Piwik --> 
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://www.nantes-roller.com/piwik/" : "http://www.nantes-roller.com/piwik/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 1);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://www.nantes-roller.com/piwik/piwik.php?idsite=1" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6022809-1', 'auto');
  ga('send', 'pageview');

</script>


</body>
</html>
