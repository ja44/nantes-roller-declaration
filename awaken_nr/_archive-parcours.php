<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Awaken
 */

get_header(); ?>
<div class="row">
<?php is_rtl() ? $rtl = 'awaken-rtl' : $rtl = ''; ?>
<div class="col-xs-12 col-sm-6 col-md-8 <?php echo $rtl ?>">
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="archive-page-header">
				<h1 class="archive-page-title">
					Les parcours
				</h1>
			</header><!-- .page-header -->
          

                   <div>
Les parcours proposés par Nantes-Roller sont au nombre de 14 : douze classiques et deux évènementiels.<br />
Leurs niveaux de difficulté sont volontairement variés. <br />
Certains, comme le N°1 ou encore le 9, ne sont pas conseillés aux débutants...alors que le 2, ou encore le 12, leur sont plus accessibles...mais aucun ne leur est véritablement destiné. Maîtrise du freinage du virage et de la vitesse sont indispensables. <br />
<br />
Vous trouverez ci-dessous des infos plus précises concernant ces parcours, ainsi que sur le forum dans le sujet "Les infos Nantes-Roller, Niveaux des parcours"<br />
<br />
<b>Nous avons besoin d'au moins 31 bénévoles à chaque rando pour pouvoir la démarrer. Merci à tous ceux qui veulent bien nous aider d'être présents dès 20h30 pour qu'on puisse partir à 20h45.</b><br /><br />
</div>

			<div class="row">
			<?php
			$counter = 0;
			
			$args = array(
				'post_type' => 'parcours',
				'orderby' => 'parcours_num',
				'order'   => 'ASC',
				'nopaging' => true
			);
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) : $query->the_post(); 

				/* Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content_parcours', get_post_format() );
				
				 $counter++;
					if ($counter % 2 == 0) {
						echo '</div><div class="row">';
				 	} 
			
			 endwhile; ?>

		</div><!-- .row -->

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

</div><!-- .bootstrap cols -->
<div class="col-xs-12 col-sm-6 col-md-4">
	<?php get_sidebar(); ?>
</div><!-- .bootstrap cols -->
</div><!-- .row -->
<?php get_footer(); ?>
