<?php

/**
 * Custom slider and the featured posts for the theme.
 */

if ( !function_exists( 'awaken_featured_posts' ) ) :

    function awaken_featured_posts() {

        global $awaken_options;

		$images = get_posts(
			array(
				'post_type'      => 'attachment',
				'numberposts'    => -1, // show all
				'post_status'    => null,
				'tax_query' => array(
					array(
						'taxonomy' => 'media_category',
						'field' => 'slug',
						'terms' => 'carrousel'
					)
				),
				'post_mime_type' => 'image',
				'orderby'        => 'rand'
			)
		); 

		
		?>

        <div class="awaken-featured-container">
            <div class="awaken-featured-slider">
                <section class="slider">
                    <div class="flexslider">
                        <ul class="slides">

							<?php foreach($images as $image) { ?>
								
                                <li>
                                    <div class="awaken-slider-container">
										<?php 
											$tImg = wp_get_attachment_image_src($image->ID, 'featured-slider');
											echo '<img src="'.$tImg[0].'" />';
										?>
                                    </div>
									<div class="awaken-slider-details-container">
                                            <h1 class="awaken-slider-title">
											<?php if($image->post_excerpt != "") {
												echo nl2br($image->post_excerpt);
											} else {
												echo "La rando du jeudi !<br>20h30 place Ricordeau";
											}
											
											 ?></h1>
                                        </div>
                                </li>

                            <?php } ?>
                        </ul>
                    </div>
                </section>
            </div><!-- .awaken-slider -->
            <div class="awaken-featured-posts">
                <?php

                $fposts_category = get_theme_mod( 'featured_posts_category', '' );
			
                $fposts = new WP_Query( array(
                    'posts_per_page' => 2,
                    'tax_query' => array(
					array(
						'taxonomy' => 'category',
						'field' => 'slug',
						'terms' => 'featured'
					)
				),
                    'ignore_sticky_posts' => 1,
					'orderby' => 'date',
					'order'   => 'DESC'
                ));

                while( $fposts->have_posts() ) : $fposts->the_post(); ?>

                    <div class="afp">
                        <figure class="afp-thumbnail"><?php echo $fposts_category; ?>
                            <?php if ( has_post_thumbnail() ) { ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail( 'featured', array('title' => get_the_title()) ); ?></a>
                            <?php } else { ?>
                                <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><img  src="<?php echo get_template_directory_uri(); ?>/images/featured.jpg" alt="<?php the_title(); ?>" /></a>
                            <?php } ?>
                        </figure>
                        <div class="afp-title">
                            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                        </div>
                    </div>

                <?php endwhile; ?>

            </div>
        </div>
    <?php
    }

endif;