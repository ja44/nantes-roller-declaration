<?php
/**
 * Awaken functions and definitions
 *
 * @package Awaken
 */


add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

if ( file_exists( get_stylesheet_directory() . '/inc/options/admin-config.php') ) {
	require_once( get_stylesheet_directory() . '/inc/options/admin-config.php' );
}

/* Load slider */
require get_stylesheet_directory() . '/inc/functions/slider.php';
require get_stylesheet_directory() . '/inc/widgets/nr_partner.php';

/**
 * Custom template tags for this theme.
 */
//require get_template_directory() . '/inc/template-tags.php';

function add_js_scripts() {
	//wp_enqueue_script( 'script', get_template_directory_uri().'/js/script.js', array('jquery'), '1.0', true );

	// pass Ajax Url to script.js
	wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
}
add_action('wp_enqueue_scripts', 'add_js_scripts');

add_action( 'wp_ajax_mon_action', 'mon_action' );
add_action( 'wp_ajax_nopriv_mon_action', 'mon_action' );

function mon_action() {

	$param = $_POST['param'];

	global $wpdb;

	
	// Récupère la liste des parcours avec le num et le champ parcours_tan
	$query = "SELECT meta_value
			FROM wp_postmeta
			WHERE meta_key = 'parcours_tan'
			AND post_id = %d";
	
	$point_tan = $wpdb->get_var($wpdb->prepare($query, $param));
			
	
	
	echo $point_tan;

	die();
}


function awaken_nr_custom_excerpt($text) {
    $excerpt = '' . strip_tags($text) . '<a class="moretag" href="'. get_permalink() . '"> ' . wp_kses_post( get_theme_mod( 'read_more_text', '[...]' ) ) . '</a>';
   	return $excerpt;
}