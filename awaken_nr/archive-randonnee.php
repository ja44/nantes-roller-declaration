<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Awaken
 */

get_header(); ?>
<div class="row">
<?php is_rtl() ? $rtl = 'awaken-rtl' : $rtl = ''; ?>
<div class="col-xs-12 col-sm-6 col-md-8 <?php echo $rtl ?>">
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="archive-page-header">
				<h1 class="archive-page-title">
					<?php
							_e( 'Liste des précédentes randonnées', 'awaken' );
					?>
				</h1>

			</header><!-- .page-header -->
            <?php
                // Show an optional term description.
                $term_description = term_description();
                if ( ! empty( $term_description ) ) :
                    printf( '<div class="taxonomy-description">%s</div>', $term_description );
                endif;
            ?>
			<?php /* Start the Loop */
				$counter = 0;
			 ?>
			<div class="row">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					$url_thumbnail_post = get_the_post_thumbnail_url(get_post()->ID, 'medium' );
					if(!isset($url_thumbnail_post) || $url_thumbnail_post == "") {
						$url_thumbnail_post = get_the_post_thumbnail_url(@get_post_meta(get_post()->ID, 'rando_parcours', true), 'medium' );
					}
					//echo "<div><a href='".esc_url( get_permalink() )."'><img src='".$url_thumbnail_post."' style='max-height:200px;' /></a></div>";
					get_template_part( 'content', get_post_format() );
					/*echo "<div id='rando_content' style='width:500px;'>";
					the_content("\nLire le détail de la randonnée...");
					echo "</div>";*/
				?>
				<?php $counter++;
					if ($counter % 2 == 0) {
						echo '</div><div class="row">';
				 	} 
				?>
			<?php endwhile; ?>

			<div class="col-xs-12 col-sm-12 col-md-12">
				<?php awaken_paging_nav(); ?>
			</div>
		</div><!-- .row -->

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

</div><!-- .bootstrap cols -->
<div class="col-xs-12 col-sm-6 col-md-4">
	<?php get_sidebar(); ?>
</div><!-- .bootstrap cols -->
</div><!-- .row -->
<?php get_footer(); ?>
