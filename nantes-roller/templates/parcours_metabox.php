<table style="width:100%">
	<tr valign="top">
		<th class="metabox_label_column" style="width:100px; text-align:right;">
			<label for="parcours_num">Numéro : </label>
		</th>
		<td>
			<select id="parcours_num" name="parcours_num">
				<option value=""></option>
<?php
$parcours_num = get_post_meta($post->ID, 'parcours_num', true);

for ($i = 1; $i <= 20; $i++) {
	if ($parcours_num == $i) {
		echo '<option value="' .
			$i .
			'" selected="selected">' .
			$i .
			'</option>';
	} else {
		echo '<option value="' . $i . '">' . $i . '</option>';
	}
}
?>
			</select>
		</td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column" style="width:100px; text-align:right;">
            <label for="parcours_distance">Distance : </label>
        </th>
        <td><input type="text" id="parcours_distance" name="parcours_distance" size="4" value="
					<?php echo @get_post_meta($post->ID, 'parcours_distance', true); ?>" /> km
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column" style="width:100px; text-align:right;">
            <label for="parcours_tan">Point TAN : </label>
        </th>
        <td>
			<?php
   $settings = array(
   	'textarea_name' => 'parcours_tan',
   	'textarea_rows' => 5,
   	'teeny' => true,
   	'media_buttons' => false
   );
   wp_editor(
   	get_post_meta($post->ID, 'parcours_tan', true),
   	'parcours_tan',
   	$settings
   );
   ?>
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column" style="width:100px; text-align:right;">
            <label for="parcours_descriptif">Descriptif : </label>
        </th>
        <td>
			<?php
   $file = @get_post_meta($post->ID, 'parcours_descriptif', true);
   $url = parse_url($file['url']);
   $file_name = basename($url['path']);
   ?>
			<input type="file" id="parcours_descriptif" name="parcours_descriptif" value="" size="25" /><br />
			<a href="<?php echo $file[
   	'url'
   ]; ?>" target="_blank"><?php echo $file_name; ?></a>
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column" style="width:100px; text-align:right;">
            <label for="parcours_plan">Plan : </label>
        </th>
        <td>
			<?php
   $file = @get_post_meta($post->ID, 'parcours_plan', true);
   $url = parse_url($file['url']);
   $file_name = basename($url['path']);
   ?>
			<input type="file" id="parcours_plan" name="parcours_plan" value="" size="25" /><br />
			<a href="<?php echo $file[
   	'url'
   ]; ?>" target="_blank"><?php echo $file_name; ?></a>
        </td>
    </tr>

</table>
