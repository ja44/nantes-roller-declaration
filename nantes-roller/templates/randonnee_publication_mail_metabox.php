
<table style="width:100%">
	<tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_publication_mail">Envoyer la déclaration par mail :</label>
        </th>
        <td>
            <input type="checkbox" id="rando_publication_mail" name="rando_publication_mail" checked="checked" />
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_publication_groupe">Liste :</label>
        </th>
        <td>
			<select id="rando_publication_groupe" name="rando_publication_groupe">
				 <?php foreach ($liste_groupes as $groupe) {
     	if ($groupe->slug == 'declaration_rando') {
     		echo '<option value="' .
     			$groupe->slug .
     			'" selected="selected">' .
     			$groupe->name .
     			'</option>';
     	} else {
     		echo '<option value="' .
     			$groupe->slug .
     			'">' .
     			$groupe->name .
     			'</option>';
     	}
     } ?>
			</select>
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_point_tan">Point TAN : </label>
        </th>
        <td>
			<?php
   $settings = array(
   	'textarea_name' => 'rando_point_tan',
   	'textarea_rows' => 5,
   	'teeny' => false,
   	'media_buttons' => false
   );
   wp_editor('', 'rando_point_tan', $settings);
   ?>
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_mail">Mail : </label>
        </th>
        <td>
			<?php
   $settings = array(
   	'textarea_name' => 'rando_mail',
   	'teeny' => false,
   	'media_buttons' => false
   );
   wp_editor(get_site_option('nr_mail'), 'rando_mail', $settings);
   ?>
        </td>
    </tr>

</table>
