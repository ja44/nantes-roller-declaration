<script type="text/javascript">
	var parcours_tan = [];

	function nr_defined_title() {

		var rando_date = document.getElementById("rando_date").value;
		var rando_parcours = document.getElementById("rando_parcours").selectedOptions[0].textContent;



		if(rando_parcours != "") {

			var parcours_id = document.getElementById("rando_parcours").value;

			jQuery.post(
				ajaxurl,
				{
					'action': 'mon_action',
					'param': parcours_id
				},
				function(response){
						tinyMCE.get('rando_point_tan').setContent(response);
					}
			);

			if(rando_date != "" && rando_parcours != "") {
				var day = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
				var month = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];

				var tabDate=rando_date.split('/')

				var d = new Date(tabDate[2], tabDate[1] -1, tabDate[0]);

				var dateFormated = day[d.getDay()] + ' ' + d.getDate() + ' ' + month[d.getMonth()] + ' '+ d.getFullYear();

				document.getElementById("title-prompt-text").innerHTML = "";
				document.getElementById("title").value = dateFormated + " - " + rando_parcours;
				document.getElementById("rando_date_string").value = dateFormated;

			}
		}


	}


</script>

<table style="width:100%">
    <tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_date">Date (au format JJ/MM/AAAA) : </label>
        </th>
        <td>
            <input type="text" id="rando_date" name="rando_date" value="<?php echo @get_post_meta(
            	$post->ID,
            	'rando_date',
            	true
            ); ?>" onChange="nr_defined_title()" />
			<input type="text" id="rando_date_string" name="rando_date_string" value="" readonly="readonly" style=" color: black; border: 0px; font-weight: bold;"/>
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_parcours">Parcours : </label>
        </th>
        <td>
            <select id="rando_parcours" name="rando_parcours" onChange="nr_defined_title()">
				<option value=""></option>
			<?php foreach ($liste_parcours as $parcours) {
   	if ($parcours->id == @get_post_meta($post->ID, 'rando_parcours', true)) {
   		echo '<option value="' .
   			$parcours->id .
   			'" selected="selected">' .
   			$parcours->post_title .
   			'</option>';
   	} else {
   		echo '<option value="' .
   			$parcours->id .
   			'">' .
   			$parcours->post_title .
   			'</option>';
   	}
   } ?>
			</select>
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_previsions">Prévisions : </label>
        </th>
        <td>
            <input type="text" name="rando_previsions" value="<?php echo @get_post_meta(
            	$post->ID,
            	'rando_previsions',
            	true
            ); ?>" />
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_responsable_rando">Coordinateur·rice de la randonnée : </label>
        </th>
        <td>
            <select name="rando_responsable_rando">
			<?php foreach ($liste_responsable_rando->posts as $resp) {
   	// Gerard Tanniou sélectionné par défaut
   	$rando_resp_rando = @get_post_meta(
   		$resp->ID,
   		'rando_responsable_rando',
   		true
   	);
   	if (
   		($rando_resp_rando == null && $resp->ID == 672) ||
   		$resp->ID == rando_resp_rando
   	) {
   		echo '<option value="' .
   			$resp->ID .
   			'" selected="selected">' .
   			get_the_title($resp->ID) .
   			'</option>';
   	} else {
   		echo '<option value="' .
   			$resp->ID .
   			'">' .
   			get_the_title($resp->ID) .
   			'</option>';
   	}
   } ?>
			</select>
        </td>
    </tr>
		<tr valign="top">
	        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
	            <label for="rando_responsable_cortege">Responsable du cortège : </label>
	        </th>
	        <td>
	            <select name="rando_responsable_cortege">
				<?php foreach ($liste_responsable_staffeurs->posts as $resp) {
    	if (
    		$resp->ID ==
    		@get_post_meta($post->ID, 'rando_responsable_cortege', true)
    	) {
    		echo '<option value="' .
    			$resp->ID .
    			'" selected="selected">' .
    			get_the_title($resp->ID) .
    			'</option>';
    	} else {
    		echo '<option value="' .
    			$resp->ID .
    			'">' .
    			get_the_title($resp->ID) .
    			'</option>';
    	}
    } ?>
				</select>
	        </td>
	    </tr>
	<tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_responsable_signaleur">Responsable des signaleur·se·s : </label>
        </th>
        <td>
            <select name="rando_responsable_signaleurs">
			<?php foreach ($liste_responsable_signaleurs->posts as $resp) {
   	if (
   		$resp->ID ==
   		@get_post_meta($post->ID, 'rando_responsable_signaleurs', true)
   	) {
   		echo '<option value="' .
   			$resp->ID .
   			'" selected="selected">' .
   			get_the_title($resp->ID) .
   			'</option>';
   	} else {
   		echo '<option value="' .
   			$resp->ID .
   			'">' .
   			get_the_title($resp->ID) .
   			'</option>';
   	}
   } ?>
			</select>
        </td>
    </tr>
	<tr valign="top">
        <th class="metabox_label_column nr" style="width:250px; text-align:right;">
            <label for="rando_responsable_secouristeouriste">Responsable des secouristes : </label>
        </th>
        <td>
			<select name="rando_responsable_secouristes">
			<?php foreach ($liste_responsable_secouristes->posts as $resp) {
   	if (
   		$resp->ID ==
   		@get_post_meta($post->ID, 'rando_responsable_secouriste', true)
   	) {
   		echo '<option value="' .
   			$resp->ID .
   			'" selected="selected">' .
   			get_the_title($resp->ID) .
   			'</option>';
   	} else {
   		echo '<option value="' .
   			$resp->ID .
   			'">' .
   			get_the_title($resp->ID) .
   			'</option>';
   	}
   } ?>
			</select>
        </td>
    </tr>
		<tr valign="top">
			<th class="metabox_label_column nr" style="width:250px; text-align:right;">
				<label for="rando_bike1">Cycliste n°1 : </label>
			</th>
			<td>
				<select name="rando_bike1">
<?php foreach ($bikes->posts as $bike) {
	$selectOption = '';

	if ($bike->ID == @get_post_meta($post->ID, 'rando_bike1', true)) {
		$selectOption = 'selected="selected"';
	}
	$bike_title = get_the_title($bike->ID);

	echo "<option value=\"{$bike->ID}\" $selectOption>$bike_title</option>";
} ?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th class="metabox_label_column nr" style="width:250px; text-align:right;">
				<label for="rando_bike2">Cycliste n°2 : </label>
			</th>
			<td>
				<select name="rando_bike2">
<?php foreach ($bikes->posts as $bike) {
	$selectOption = '';

	if ($bike->ID == @get_post_meta($post->ID, 'rando_bike2', true)) {
		$selectOption = 'selected="selected"';
	}

	$bike_title = get_the_title($bike->ID);

	echo "<option value=\"{$bike->ID}\" $selectOption>$bike_title</option>";
} ?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<th class="metabox_label_column nr" style="width:250px; text-align:right;">
				<label for="rando_driver">Conducteur·rice : </label>
			</th>
			<td>
				<select name="rando_driver">
<?php foreach ($drivers->posts as $driver) {
	$selectOption = '';

	if ($driver->ID == @get_post_meta($post->ID, 'rando_driver', true)) {
		$selectOption = 'selected="selected"';
	}

	$driver_title = get_the_title($driver->ID);

	echo "<option value=\"{$driver->ID}\" $selectOption>$driver_title</option>";
} ?>
				</select>
			</td>
		</tr>
	<tr valign="top">
		<th class="metabox_label_column nr" style="width:250px; text-align:right;">

		<?php
  $pdf_with_num = @get_post_meta($post->ID, 'rando_declaration_with_num', true);
  $pdf_without_num = @get_post_meta(
  	$post->ID,
  	'rando_declaration_without_num',
  	true
  );
  if ($pdf_with_num != null) {
  	$label = 'Déclaration de randonnée';
  } else {
  	$label = 'Générer la déclaration de randonée';
  }
  ?>
			<label for="rando_declaration"><?php echo $label; ?> : </label>
        </th>
        <td>
			<?php if ($pdf_with_num != null) {
   	echo '<a href="' .
   		$pdf_with_num .
   		'" target="_blank">Déclaration de randonnée (avec les numéros)</a><br>';
   	echo '<a href="' .
   		$pdf_without_num .
   		'" target="_blank">Déclaration de randonnée (sans les numéros)</a>';
   } else {
   	echo '<input type="checkbox" name="rando_declaration_generation" checked="checked">';
   } ?>
        </td>
    </tr>
</table>
<h1>Contenu du post à créer dans le forum :</h1>
<div id="forum-content" style="width:100%; height: 200px; white-space: pre; overflow: auto;">
	<?php echo @get_post_meta($post->ID, 'forumContent', true); ?>
</div>
<script>

const forumContent = document.querySelector('#forum-content');
const range = new Range();
range.setStartBefore(forumContent);
range.setEndAfter(forumContent);
const selection = window.getSelection();
selection.addRange(range);
document.execCommand('copy');
</script>
