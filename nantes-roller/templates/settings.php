<div class="wrap">
    <h2>Paramétrage Nantes-Roller</h2>

    <form method="post" action="options.php">
		<?php settings_fields('nr_options'); ?>
		<div class="row spacer">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<label for="nr_rando">Contenu rando :</label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
				<?php
    $settings = array(
    	'textarea_name' => 'nr_rando',
    	'teeny' => false,
    	'media_buttons' => false
    );
    wp_editor(get_option('nr_rando'), 'nr_rando', $settings);
    ?>
			</div>
		</div>
		<div class="row spacer">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<label for="nr_mail">Texte par défaut pour le mail :</label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
				<em>Attention, les champs suivants doivent obligatoirement figurer : @@DATE@@, @@POINT_TAN@@</em>
				<?php
    $settings = array(
    	'textarea_name' => 'nr_mail',
    	'teeny' => false,
    	'media_buttons' => false
    );
    wp_editor(get_option('nr_mail'), 'nr_mail', $settings);
    ?>
			</div>
		</div>
		<div class="row spacer">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<label for="nr_forum">Message par défaut du forum :</label>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
				<textarea rows="15" cols="100" name="nr_forum"><?php echo get_option(
    	'nr_forum'
    ); ?></textarea>
			</div>
		</div>
        <div class="row spacer">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php @submit_button(); ?>
			</div>
		</div>
    </form>
</div>
