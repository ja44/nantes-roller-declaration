<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Date {
	var $d_date;
	var $a_mois;
	var $a_journee;
	var $a_jour_ferie;
	var $a_jour_ferie_fixe;
	var $a_jour_ferie_variable;
	var $i_annee_ferie;
	function Date($date = null) {
		//Initialisation des tableaux
		$this->a_mois = array(
			'janvier',
			'février',
			'mars',
			'avril',
			'mai',
			'juin',
			'juillet',
			'août',
			'septembre',
			'octobre',
			'novembre',
			'décembre'
		);
		$this->a_journee = array(
			'Lundi',
			'Mardi',
			'Mercredi',
			'Jeudi',
			'Vendredi',
			'Samedi',
			'Dimanche'
		);
		if ($date == null || !isset($date['date'])) {
			$this->d_date = time();
		} elseif (
			ereg(
				'^([0-9]{1,2})/([0-9]{1,2})/([0-9]{2}|[0-9]{4})',
				$date['date'],
				$regs
			)
		) {
			$this->d_date = mktime(0, 0, 0, $regs[2], $regs[1], $regs[3]);
		} elseif (
			ereg(
				'^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})',
				$date['date'],
				$regs
			)
		) {
			$this->d_date = mktime(
				$regs[4],
				$regs[5],
				$regs[6],
				$regs[2],
				$regs[3],
				$regs[1]
			);
		} elseif ($this->isValidDate($date['date'])) {
			$this->d_date = $date['date'];
		} else {
			$this->d_date = time();
		}
	}
	function getDate() {
		return $this->d_date;
	}
	function setDate($i_date) {
		$this->d_date = $i_date;
		if (
			isset($this->a_jour_ferie) &&
			date('Y', $this->d_date) != $i_annee_ferie
		) {
			$this->buildJourFerie();
		}
	}
	function setDateDifferentFormat($sDate) {
		if (
			ereg(
				'^([0-9]{1,2})/([0-9]{1,2})/([0-9]{2}|[0-9]{4})',
				$sDate,
				$regs
			)
		) {
			$this->d_date = mktime(0, 0, 0, $regs[2], $regs[1], $regs[3]);
		} elseif (
			ereg(
				'^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})',
				$sDate,
				$regs
			)
		) {
			$this->d_date = mktime(
				$regs[4],
				$regs[5],
				$regs[6],
				$regs[2],
				$regs[3],
				$regs[1]
			);
		} elseif (ereg('^([0-9]{4})-([0-9]{2})-([0-9]{2})', $sDate, $regs)) {
			$this->d_date = mktime(0, 0, 0, $regs[2], $regs[3], $regs[1]);
		} elseif ($this->isValidDate($sDate)) {
			$this->d_date = $sDate;
		} else {
			$this->d_date = time();
		}
	}

	function isValidDate($date) {
		return is_numeric($date);
	}

	function getDateFormatFr() {
		//JJ/MM/AAAA
		return date('d', $this->d_date) .
			'/' .
			date('m', $this->d_date) .
			'/' .
			date('Y', $this->d_date);
	}
	function getDateFr() {
		//jeudi 10 septembre 2009
		return $this->a_journee[date('N', $this->d_date) - 1] .
			' ' .
			date('j', $this->d_date) .
			' ' .
			$this->a_mois[date('n', $this->d_date) - 1] .
			' ' .
			date('Y', $this->d_date);
	}
	function getJour() {
		//lundi
		return $this->a_journee[date('N', $this->d_date) - 1];
	}
	function getDateNumerique() {
		//JJMMAAAA
		return date('d', $this->d_date) .
			date('m', $this->d_date) .
			date('Y', $this->d_date);
	}
	function getDateFormatUS() {
		//2009-09-10
		return date('Y', $this->d_date) .
			'-' .
			date('m', $this->d_date) .
			'-' .
			date('d', $this->d_date);
	}
	function getDateMySQL() {
		//2009-09-10 21:00:00
		return $this->date_to_mysql() .
			' ' .
			date('H', $this->d_date) .
			':' .
			date('i', $this->d_date) .
			':' .
			date('s', $this->d_date);
	}
	function getAddDayDate($nb_jours) {
		return strtotime('+' . $nb_jours . 'days', $this->d_date);
	}
	function date_to_mysql() {
		return $this->getDateFormatUS();
	}
	function get_add_day_date($nb_jours) {
		$date = $this;
		$date->add_day_date($nb_jours);
		return $date;
	}
	function add_day_date($nb_jours) {
		$this->d_date = strtotime('+' . $nb_jours . 'days', $this->d_date);
	}
	function buildJourFerie() {
		$this->a_jour_ferie_fixe = array(
			mktime(0, 0, 0, 1, 1, date('Y', $this->d_date)) => "Jour de l'an",
			mktime(
				0,
				0,
				0,
				5,
				1,
				date('Y', $this->d_date)
			) => 'Fête du travail',
			mktime(0, 0, 0, 5, 8, date('Y', $this->d_date)) => 'Armistice 1945',
			mktime(
				0,
				0,
				0,
				7,
				14,
				date('Y', $this->d_date)
			) => 'Fête nationale',
			mktime(
				0,
				0,
				0,
				11,
				11,
				date('Y', $this->d_date)
			) => 'Armistice 1918',
			mktime(0, 0, 0, 12, 25, date('Y', $this->d_date)) => 'Noël',
			mktime(0, 0, 0, 8, 15, date('Y', $this->d_date)) => 'Assomption',
			mktime(0, 0, 0, 11, 1, date('Y', $this->d_date)) => 'Toussaint'
		);
		$this->a_jour_ferie_variable = array(
			easter_date(date('Y', $this->d_date)) +
			3600 * 24 * 1 => 'Lundi de Pâques',
			easter_date(date('Y', $this->d_date)) +
			3600 * 24 * 39 => "Jeudi de l'Ascension",
			easter_date(date('Y', $this->d_date)) +
			3600 * 24 * 50 => 'Lundi de Pentecôte'
		);
		$this->a_jour_ferie =
			$this->a_jour_ferie_fixe + $this->a_jour_ferie_variable;
	}
	function getJoursFerie() {
		if (!isset($a_jour_ferie)) {
			$this->buildJourFerie();
		}
		return $this->a_jour_ferie;
	}
	function isFerie() {
		if (!isset($this->a_jour_ferie)) {
			$this->buildJourFerie();
		}
		foreach ($this->a_jour_ferie as $i_timestamp => $s_jour_ferie) {
			if (
				$this->d_date >= $i_timestamp &&
				$this->d_date < $i_timestamp + 3600 * 24
			) {
				return true;
			}
		}
		return false;
	}
	function getLibelleJourFerie() {
		if (!isset($a_jour_ferie)) {
			$this->buildJourFerie();
		}
		foreach ($a_jour_ferie as $i_timestamp => $s_jour_ferie) {
			if (
				$this->d_date >= $i_timestamp &&
				$this->d_date < $i_timestamp + 3600 * 24
			) {
				return $s_jour_ferie;
			}
		}
		return false;
	}
	function getLibelleMois($i_mois) {
		if (is_numeric($i_mois) && $i_mois > 0 && $i_mois < 13) {
			return $this->a_mois[$i_mois];
		} else {
			return false;
		}
	}
}
?>
