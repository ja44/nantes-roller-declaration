<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}
class Calendar {
	var $CI;
	function Calendar() {
		$this->CI = get_instance();
	}
	function getJSON() {
		$this->CI->load->library('date');
		$this->CI->db->order_by('cal_timestamp');
		$query = $this->CI->db->get('calendriers');
		$a_calendrier = array();
		$i = 0;
		foreach ($query->result() as $row) {
			switch ($row->cal_type) {
				case 1:
					$className = 'event-title-ferie';
					if ($row->cal_aborted) {
						$className .= ' aborted';
					}
					break;
				case 2:
					$className = 'event-title-normal';
					if ($row->cal_aborted) {
						$className .= ' aborted';
					}
					break;
				case 3:
					$className = 'event-title-evenmentiel';
					if ($row->cal_aborted) {
						$className .= ' aborted';
					}
					break;
				case 4:
					$className = 'event-title-externe';
					if ($row->cal_aborted) {
						$className .= ' aborted';
					}
					break;
				default:
					$className = 'event-title-normal';
					break;
			}
			$this->CI->date->d_date = $row->cal_timestamp;
			$a_calendrier[] = array(
				'id' => $i,
				'title' => $row->cal_libelle,
				'start' => $this->CI->date->getDateFormatUS(),
				'className' => $className
				//'url' => "http://yahoo.com/"
			);
			$i++;
		}
		echo json_encode($a_calendrier);
	}

	function create_calendar($date_debut, $parcours_debut) {
		$this->CI->load->library('date', array('date' => $date_debut));
		//Fixe la date à 21h
		$this->CI->date->setDate(
			mktime(
				21,
				0,
				0,
				date('m', $this->CI->date->getDate()),
				date('d', $this->CI->date->getDate()),
				date('Y', $this->CI->date->getDate())
			)
		);
		$parcours_current = $parcours_debut;
		$a_annee_current = date('Y', $this->CI->date->getDate());
		$d_periode_parcours_non_realisable = array(
			1 => array(
				mktime(0, 0, 0, 5, 15, $a_annee_current),
				mktime(23, 59, 59, 9, 15, $a_annee_current)
			)
		);

		//Création des jours fériés
		$a_jours_feries = $this->CI->date->getJoursFerie();
		$this->CI->db->where('cal_type', 1);
		$query = $this->CI->db->get('calendriers');
		$a_jours_feries_existant = array();
		foreach ($query->result() as $row) {
			$a_jours_feries_existant[] = $row->cal_timestamp;
		}
		foreach ($a_jours_feries as $i_timestamp => $s_fete_libelle) {
			if (!in_array($i_timestamp, $a_jours_feries_existant)) {
				//Ce jour férié n'as pas encoré été inséré
				$a_journee = array(
					'cal_timestamp' => $i_timestamp,
					'cal_libelle' => $s_fete_libelle,
					'cal_type' => 1 //Jour férié = 1
				);
				$this->CI->db->insert('calendriers', $a_journee);
			}
		}
		/*
		 * Insertion des différentes randos dans la base de données.
		 */

		//Récupération des parcours
		$query = $this->CI->db->get('parcours');
		$a_liste_parcours = array();
		foreach ($query->result() as $row) {
			$a_liste_parcours[$row->par_num] =
				'Rando Roller : Parcours n°' .
				$row->par_num .
				' (' .
				$row->par_nom .
				')';
		}

		//Fiwe la date au premier jeudi
		if (date('w', $this->CI->date->getDate()) < 4) {
			$this->CI->date->add_day_date(
				4 - date('w', $this->CI->date->getDate())
			);
		}
		if (date('w', $this->CI->date->getDate()) > 4) {
			$this->CI->date->add_day_date(
				7 + 4 - date('w', $this->CI->date->getDate())
			);
		}

		while (date('Y', $this->CI->date->getDate()) == $a_annee_current) {
			if (!$this->CI->date->isFerie()) {
				//Pas férié
				$this->CI->db->where(
					'cal_timestamp',
					$this->CI->date->getDate()
				);
				$query = $this->CI->db->get('calendriers');
				if ($query->num_rows() == 0) {
					//Pas de rando évènementiel, ce jour là
					if (!in_array($parcours_current, range(1, 12))) {
						$parcours_current = 1;
					}
					if (
						isset(
							$d_periode_parcours_non_realisable[
								$parcours_current
							]
						) &&
						$this->CI->date->getDate() >=
							$d_periode_parcours_non_realisable[
								$parcours_current
							][0] &&
						$this->CI->date->getDate() <=
							$d_periode_parcours_non_realisable[
								$parcours_current
							][1]
					) {
						$parcours_current++;
					}

					//Ajour du parcours dans la base de données
					$a_journee = array(
						'cal_timestamp' => $this->CI->date->getDate(),
						'cal_libelle' => $a_liste_parcours[$parcours_current],
						'cal_type' => 2 //Rando normal = 2
					);
					$this->CI->db->insert('calendriers', $a_journee);
					$parcours_current++;
				}
			}
			$this->CI->date->add_day_date(7);
		}
	}
}

?>
