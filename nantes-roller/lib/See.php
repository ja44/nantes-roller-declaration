<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class See {
	var $CI;
	function See() {
		$this->CI = get_instance();
	}
	function affiche($id, $user) {
		$this->CI->db->where('doc_id', $id);
		$query = $this->CI->db->get('documents');

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				if (!$row->doc_public && $user == false) {
					echo 'Vous ne pouvez pas accéder à ce contenu';
					return;
				} else {
					header('Content-type: ' . $row->doc_file_type);
					header(
						'Content-Disposition: filename=' . $row->doc_file_name
					);
					echo $row->doc_blob;
					if ($row->doc_compter) {
						$this->CI->db->set('doc_nb', 'doc_nb + 1', false);
						$this->CI->db->where('doc_id', $id);
						$this->CI->db->update('documents');
					}
				}
			}
		}
	}

	function enregistre(
		$file,
		$file_name,
		$file_type,
		$file_ext,
		$tag_name,
		$doc_compter,
		$doc_public
	) {
		//TODO : remplacer tag_name par tag_id
		$document['doc_blob'] = $file;
		$document['doc_file_type'] = $file_type;
		$document['doc_file_name'] = $file_name;
		$document['doc_tag_name'] = $tag_name;
		$document['doc_extension'] = $file_ext;
		$document['doc_compter'] = $doc_compter;
		$document['doc_public'] = $doc_public;
		$this->CI->db->insert('documents', $document);
		return $this->CI->db->insert_id();
	}

	function get($id, $path, $ext = false, $name = false) {
		$this->CI->db->where('doc_id', $id);
		$query = $this->CI->db->get('documents');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				if ($name) {
					$path .= $row->doc_file_name;
				}
				if ($ext) {
					$path .= '.' . $row->doc_extension;
				}
				$fp = fopen($path, 'w+');
				fwrite($fp, $row->doc_blob);
				fclose($fp);
				return $path;
			}
		}
	}
}
