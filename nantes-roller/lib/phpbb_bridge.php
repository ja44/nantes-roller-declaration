<?php //if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter phpBB3 Bridge
 * @author Georgi Budinov, credits to Tomaž Muraus at http://www.tomaz-muraus.info
 * @link georgi.budinov.com
 */
class Phpbb_bridge {
	protected $_user;

	/**
	 * Constructor.
	 */

	public function __construct() {
		global $phpbb_root_path,
			$phpEx,
			$user,
			$auth,
			$cache,
			$db,
			$config,
			$template,
			$table_prefix;

		define('FORUM_ROOT_PATH', 'forum/');
		define('IN_PHPBB', true);
		$phpbb_root_path = $_SERVER['DOCUMENT_ROOT'] . '/forum/';
		$phpEx = substr(strrchr(__FILE__, '.'), 1);

		$path_common = $phpbb_root_path . 'common.php';
		$path_config = $phpbb_root_path . 'config.php';
		$path_posting = $phpbb_root_path . 'includes/functions_posting.php';

		include $path_common;
		include_once $path_config;
		include_once $path_posting;

		// Initialize phpBB user session
		$user->session_begin();
		$auth = new auth();
		$auth->acl($user->data);
		$user->setup();

		// Save user data into $_user variable
		$this->_user = $user;
	}

	public function publishPhpBBForum($subject, $message, $user_post) {
		global $phpbb_root_path,
			$phpEx,
			$user,
			$auth,
			$cache,
			$db,
			$config,
			$template,
			$table_prefix;

		$sql =
			'SELECT user_id, user_ip, username, user_colour
				FROM ' .
			USERS_TABLE .
			'
				WHERE user_id = ' .
			$user_post;

		$result = $db->sql_query($sql);
		$user_row = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);

		$user->data['user_id'] = $user_row['user_id'];
		$user->data['username'] = $user_row['username'];
		$user->data['user_colour'] = $user_row['user_colour'];
		$user->data['is_registered'] = true;

		// note that multibyte support is enabled here
		$my_subject = utf8_normalize_nfc(
			request_var('my_subject', $subject, true)
		);
		$my_text = utf8_normalize_nfc(request_var('my_text', $message, true));

		// variables to hold the parameters for submit_post
		$poll = $uid = $bitfield = $options = '';

		generate_text_for_storage(
			$my_subject,
			$uid,
			$bitfield,
			$options,
			false,
			false,
			false
		);
		generate_text_for_storage(
			$my_text,
			$uid,
			$bitfield,
			$options,
			true,
			true,
			true
		);

		$data = array(
			// General Posting Settings
			'forum_id' => 20, // The forum ID in which the post will be placed. (int)
			'topic_id' => 0, // Post a new topic or in an existing one? Set to 0 to create a new one, if not, specify your topic ID here instead.
			'icon_id' => false, // The Icon ID in which the post will be displayed with on the viewforum, set to false for icon_id. (int)

			// Defining Post Options
			'enable_bbcode' => true, // Enable BBcode in this post. (bool)
			'enable_smilies' => true, // Enabe smilies in this post. (bool)
			'enable_urls' => true, // Enable self-parsing URL links in this post. (bool)
			'enable_sig' => true, // Enable the signature of the poster to be displayed in the post. (bool)

			// Message Body
			'message' => $my_text, // Your text you wish to have submitted. It should pass through generate_text_for_storage() before this. (string)
			'message_md5' => md5($my_text), // The md5 hash of your message

			// Values from generate_text_for_storage()
			'bbcode_bitfield' => $bitfield, // Value created from the generate_text_for_storage() function.
			'bbcode_uid' => $uid, // Value created from the generate_text_for_storage() function.

			// Other Options
			'post_edit_locked' => 0, // Disallow post editing? 1 = Yes, 0 = No
			'topic_title' => $my_subject, // Subject/Title of the topic. (string)

			// Email Notification Settings
			'notify_set' => false, // (bool)
			'notify' => false, // (bool)
			'post_time' => 0, // Set a specific time, use 0 to let submit_post() take care of getting the proper time (int)
			'forum_name' => '', // For identifying the name of the forum in a notification email. (string)

			// Indexing
			'enable_indexing' => true, // Allow indexing the post? (bool)

			// 3.0.6
			'force_approved_state' => true // Allow the post to be submitted without going into unapproved queue
		);

		$post_id = submit_post(
			'post',
			$my_subject,
			$user_post,
			POST_NORMAL,
			$poll,
			$data
		);

		return $post_id;
	}
}
