<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.natens-roller.com
 * @since      1.0
 *
 * @package    Nantes-Roller
 * @subpackage Nantes-Roller/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Nantes-Roller
 * @subpackage Nantes-Roller_List/admin
 * @author     Patrick Raguin <patrick.raguin@gmail.com>
 */
class NantesRollerAdmin {
	/**
	 * The Constructor
	 */
	public function __construct() {
		// register actions
		add_action('admin_menu', array($this, 'add_admin_menu'));
	}

	/**
	 * Administration
	 */
	public function add_admin_menu() {
		add_menu_page(
			'Administration Nantes-Roller',
			'Admin NR',
			'manage_options',
			'nantes-roller',
			array($this, 'admin_nr')
		);
		add_action('admin_init', array($this, 'register_settings'));
		add_action('admin_enqueue_scripts', array(
			$this,
			'load_custom_wp_admin_style'
		));
	}

	function load_custom_wp_admin_style($hook_suffix) {
		if ($hook_suffix == 'toplevel_page_nantes-roller') {
			wp_enqueue_style(
				'admin_css_bootstrap',
				plugins_url('/nantes-roller/css/bootstrap.min.css'),
				false,
				'1.0.0',
				'all'
			);
			wp_enqueue_style(
				'admin_css_style',
				plugins_url('/nantes-roller/css/style.css')
			);
		}
	}

	public function admin_nr() {
		include sprintf('%s/../templates/settings.php', dirname(__FILE__));
	}

	public function register_settings() {
		register_setting('nr_options', 'nr_forum');
		register_setting('nr_options', 'nr_mail');
		register_setting('nr_options', 'nr_rando');
	}
}
