<?php
namespace Members;

function getMembersByGroup($group) {
	return $args = array(
		'post_type' => 'staff-member',
		'orderby' => 'title',
		'order' => 'ASC',
		'nopaging' => true,
		'tax_query' => array(
			array(
				'taxonomy' => 'staff-member-group',
				'field' => 'slug',
				'terms' => $group
			)
		)
	);
}

?>
