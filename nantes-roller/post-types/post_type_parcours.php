<?php
// jare: replace by
require_once plugin_dir_path(__FILE__) . '../lib/fpdf.php';
require_once plugin_dir_path(__FILE__) . 'members.ns.php';
require_once plugin_dir_path(__FILE__) . 'groups.ns.php';

if (!class_exists('PostTypeParcours')) {
	/**
	 * A PostTypeTemplate class that provides 3 additional meta fields
	 */
	class PostTypeParcours {
		const POST_TYPE = 'parcours';

		/**
		 * The Constructor
		 */
		public function __construct() {
			// register actions
			add_action('init', array(&$this, 'init'));
			add_action('admin_init', array(&$this, 'admin_init'));
		}

		/**
		 * hook into WP's init action hook
		 */
		public function init() {
			// Initialize Post Type
			$this->create_post_type();
			add_action('save_post', array(&$this, 'save_post'));
			add_action('post_edit_form_tag', array(&$this, 'update_edit_form'));
		}

		/**
		 * METABOX
		 */
		public function admin_init() {
			// Add metaboxes
			add_action('add_meta_boxes', array(&$this, 'add_meta_boxes'));
		}

		public function add_meta_boxes() {
			// Add this metabox to every selected post
			add_meta_box(
				'nr_parcours_meta_box',
				'Infos complémentaires',
				array(&$this, 'add_inner_meta_boxes'),
				self::POST_TYPE
			);
		}

		public function add_inner_meta_boxes($post) {
			// Render the job order metabox
			include sprintf(
				'%s/../templates/%s_metabox.php',
				dirname(__FILE__),
				self::POST_TYPE
			);
		}

		function update_edit_form() {
			echo ' enctype="multipart/form-data"';
		}

		/**
		 * Create the post type
		 */
		public function create_post_type() {
			register_post_type(self::POST_TYPE, array(
				'labels' => array(
					'name' => 'Parcours',
					'singular_name' => 'Parcours',
					'all_items' => 'Parcours',
					'add_new_item' => 'Ajouter un parcours',
					'edit_item' => 'Éditer un parcours',
					'new_item' => 'Nouveau parcours',
					'view_item' => 'Voir le parcours',
					'search_items' => 'Rechercher parmi les parcours',
					'not_found' => 'Pas de parcours trouvé',
					'not_found_in_trash' => 'Pas de parcours dans la corbeille',
					'parent' => 'Rando Roller du jeudi'
				),
				'public' => true,
				'has_archive' => false,
				'hierarchical' => false,
				/*'rewrite' => array(
						'slug' => 'rando-roller-du-jeudi/parcours',
					),*/
				'description' => __(''),
				'supports' => array('title', 'editor', 'thumbnail')
			));
			add_shortcode('liste_parcours', array(
				&$this,
				'shortcode_liste_parcours'
			));
		}

		public function shortcode_liste_parcours($param, $content) {
			$parcours = '<div class="row">';

			$counter = 0;

			$args = array(
				'post_type' => 'parcours',
				'orderby' => 'parcours_num',
				'order' => 'ASC',
				'nopaging' => true
			);
			$query = new WP_Query($args);
			while ($query->have_posts()):
				$query->the_post();

				/* Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				//get_template_part( 'content_parcours', get_post_format() );

				$parcours .= '<div class="col-xs-12 col-sm-6 col-md-6">';
				$parcours .=
					'<article id="post-' .
					get_the_ID() .
					'" class="post-' .
					get_the_ID() .
					' genaral-post-item parcours type-parcours status-publish has-post-thumbnail hentry">';

				$parcours .= '<header class="genpost-entry-header">';
				$parcours .=
					'<h1 class="genpost-entry-title"><a href="' .
					esc_url(get_permalink()) .
					'" rel="bookmark">' .
					get_the_title() .
					'</a></h1>';
				$parcours .= '</header>';

				$parcours .= '<div class="genpost-entry-content">';
				if (has_post_thumbnail()) {
					$url_thumbnail_post = get_the_post_thumbnail_url(
						get_the_ID(),
						'medium'
					);

					$parcours .=
						'<figure class="e"><div><a href="' .
						esc_url(get_permalink()) .
						'"><img src="' .
						$url_thumbnail_post .
						'" /></a></div></figure>';
					/* <figure class="e"><div><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'medium' ); ?></a></div></figure>*/
				} else {
					$parcours .=
						'<figure class="e"><div><a href="' .
						esc_url(get_permalink()) .
						'"><img src="' .
						get_template_directory_uri() .
						'/images/thumbnail-default.jpg" /></a></div></figure>';
				}

				$parcours .= '</div>';
				$parcours .= '</article>';
				$parcours .= '</div>';

				$counter++;
				if ($counter % 2 == 0) {
					$parcours .= '</div><div class="row">';
				}
			endwhile;

			$parcours .= '</div>';

			// use reset postdata to restore orginal query
			wp_reset_postdata();

			return $parcours;
		}

		/**
		 * Save the metaboxes for this custom post type
		 */
		public function save_post($post_id) {
			// Make sure the file array isn't empty
			if (isset($_POST['parcours_num'])) {
				update_post_meta(
					$post_id,
					'parcours_num',
					esc_html($_POST['parcours_num'])
				);
			} else {
				delete_post_meta($post_id, 'parcours_num');
			}

			if (isset($_POST['parcours_distance'])) {
				update_post_meta(
					$post_id,
					'parcours_distance',
					esc_html($_POST['parcours_distance'])
				);
			} else {
				delete_post_meta($post_id, 'parcours_distance');
			}

			if (isset($_POST['parcours_tan'])) {
				update_post_meta(
					$post_id,
					'parcours_tan',
					$_POST['parcours_tan']
				);
			} else {
				delete_post_meta($post_id, 'parcours_tan');
			}

			if (!empty($_FILES['parcours_descriptif']['name'])) {
				// Setup the array of supported file types. In this case, it's just PDF.
				$supported_types = array('application/pdf');

				// Get the file type of the upload
				$arr_file_type = wp_check_filetype(
					basename($_FILES['parcours_descriptif']['name'])
				);
				$uploaded_type = $arr_file_type['type'];

				// Check if the type is supported. If not, throw an error.
				if (in_array($uploaded_type, $supported_types)) {
					// Use the WordPress API to upload the file
					$upload = wp_upload_bits(
						$_FILES['parcours_descriptif']['name'],
						null,
						file_get_contents(
							$_FILES['parcours_descriptif']['tmp_name']
						)
					);

					if (isset($upload['error']) && $upload['error'] != 0) {
						wp_die(
							'There was an error uploading your file. The error is: ' .
								$upload['error']
						);
					} else {
						//add_post_meta($id, 'parcours_descriptif', $upload);
						update_post_meta(
							$post_id,
							'parcours_descriptif',
							$upload
						);
					}
				} else {
					wp_die("The file type that you've uploaded is not a PDF.");
				}
			}

			if (!empty($_FILES['parcours_plan']['name'])) {
				// Setup the array of supported file types. In this case, it's just PDF.
				$supported_types = array(
					'image/gif',
					'image/jpeg',
					'image/pjpeg',
					'image/png'
				);

				// Get the file type of the upload
				$arr_file_type = wp_check_filetype(
					basename($_FILES['parcours_plan']['name'])
				);
				$uploaded_type = $arr_file_type['type'];

				// Check if the type is supported. If not, throw an error.
				if (in_array($uploaded_type, $supported_types)) {
					$upload = wp_upload_bits(
						$_FILES['parcours_plan']['name'],
						null,
						file_get_contents($_FILES['parcours_plan']['tmp_name'])
					);

					if (isset($upload['error']) && $upload['error'] != 0) {
						wp_die(
							'There was an error uploading your file. The error is: ' .
								$upload['error']
						);
					} else {
						update_post_meta($post_id, 'parcours_plan', $upload);
					}
				} else {
					wp_die(
						"The file type that you've uploaded is not an image."
					);
				}
			}
		}
	}
}

if (!class_exists('PostTypeRando')) {
	/**
	 * A PostTypeTemplate class that provides 3 additional meta fields
	 */
	class PostTypeRando {
		const POST_TYPE = 'randonnee';

		/**
		 * The Constructor
		 */
		public function __construct() {
			// register actions
			add_action('init', array(&$this, 'init'));
			add_action('admin_init', array(&$this, 'admin_init'));
		}

		/**
		 * hook into WP's init action hook
		 */
		public function init() {
			// Initialize Post Type
			$this->create_post_type();
			add_action('save_post', array(&$this, 'save_post'));
			add_filter(
				'default_content',
				array(&$this, 'rando_preset_editor_content'),
				100,
				2
			);
			add_action('the_content', array(&$this, 'show_content_single'));
		}

		function show_content_single($content) {
			if (is_single() && get_post_type() == 'randonnee') {
				$content_hr =
					"<hr style='border: 0; height: 1px; background: #333; background-image: linear-gradient(to right, #ccc, #333, #ccc);' /><h3>Détail du parcours</h3>";

				$page_data = get_post(get_post()->rando_parcours);
				$retour =
					$content . $content_hr . wpautop($page_data->post_content);

				return $retour;
			} else {
				return $content;
			}
		}

		/**
		 * Create the post type
		 */
		public function create_post_type() {
			register_post_type(self::POST_TYPE, array(
				'labels' => array(
					'name' => 'Randonnee',
					'singular_name' => 'Randonnee',
					'all_items' => 'Randonnées',
					'add_new_item' => 'Ajouter une randonnée',
					'edit_item' => 'Éditer une randonnée',
					'new_item' => 'Nouvelle randonnée',
					'view_item' => 'Voir la randonnée',
					'search_items' => 'Rechercher parmi les randonnées',
					'not_found' => 'Pas de randonnée trouvée',
					'not_found_in_trash' => 'Pas de randonnée dans la corbeille'
				),
				'public' => true,
				'has_archive' => true,
				'description' => __(
					'This is a sample post type meant only to illustrate a preferred structure of plugin development'
				),
				'supports' => array('title', 'editor', 'thumbnail')
			));
		}

		/**
		 * METABOX
		 */
		public function admin_init() {
			// Add metaboxes
			add_action('add_meta_boxes', array(&$this, 'add_meta_boxes'));
		}

		public function add_meta_boxes() {
			add_meta_box(
				'nr_info_rando_meta_box',
				'Informations',
				array(&$this, 'add_inner_infos_meta_boxes'),
				self::POST_TYPE
			);
			add_meta_box(
				'nr_publication_mail_rando_meta_box',
				'Déclaration de randonnée par mail',
				array(&$this, 'nr_publication_mail_rando_meta_box'),
				self::POST_TYPE
			);
		}

		public function add_inner_infos_meta_boxes($post) {
			global $wpdb;

			// Récupère la liste des parcours avec le num et le champ parcours_tan
			$query = "SELECT p.id, p.post_title, ptan.meta_value
					FROM wp_posts p
					left join (select post_id, meta_value from wp_postmeta where meta_key = 'parcours_num') pnum on pnum.post_id = p.id
					left join (select post_id, meta_value from wp_postmeta where meta_key = 'parcours_tan') ptan on ptan.post_id = p.id
					where post_type = 'parcours'
					and post_status = 'publish'
					order by CONVERT(SUBSTRING_INDEX(pnum.meta_value,'-',-1),UNSIGNED INTEGER)";

			$liste_parcours = $wpdb->get_results($query);

			$args = array(
				'post_type' => 'staff-member',
				'orderby' => 'title',
				'order' => 'ASC',
				'nopaging' => true,
				'tax_query' => array(
					array(
						'taxonomy' => 'staff-member-group',
						'field' => 'slug',
						'terms' => ID_RESP_RANDO
					)
				)
			);

			$liste_responsable_rando = new WP_Query($args);
			$args = array(
				'post_type' => 'staff-member',
				'orderby' => 'title',
				'order' => 'ASC',
				'nopaging' => true,
				'tax_query' => array(
					array(
						'taxonomy' => 'staff-member-group',
						'field' => 'slug',
						'terms' => ID_RESP_SIGN
					)
				)
			);
			$liste_responsable_signaleurs = new WP_Query($args);

			$args = array(
				'post_type' => 'staff-member',
				'orderby' => 'title',
				'order' => 'ASC',
				'nopaging' => true,
				'tax_query' => array(
					array(
						'taxonomy' => 'staff-member-group',
						'field' => 'slug',
						'terms' => ID_RESP_STAFF
					)
				)
			);
			$liste_responsable_staffeurs = new WP_Query($args);

			$args = array(
				'post_type' => 'staff-member',
				'orderby' => 'title',
				'order' => 'ASC',
				'nopaging' => true,
				'tax_query' => array(
					array(
						'taxonomy' => 'staff-member-group',
						'field' => 'slug',
						'terms' => ID_RESP_SEC
					)
				)
			);
			$liste_responsable_secouristes = new WP_Query($args);

			$bikes = new WP_Query(Members\getMembersByGroup(Groups\VELOS_ID));
			$drivers = new WP_Query(
				Members\getMembersByGroup(Groups\DRIVERS_ID)
			);

			// Render the job order metabox
			include sprintf(
				'%s/../templates/%s_infos_metabox.php',
				dirname(__FILE__),
				self::POST_TYPE
			);

			// use reset postdata to restore orginal query
			wp_reset_postdata();
		}

		public function nr_publication_mail_rando_meta_box($post) {
			global $wpdb;

			// Render the job order metabox
			$mail_envoye = @get_post_meta($post->ID, 'rando_mail', true);

			$liste_groupes = $wpdb->get_results("SELECT name, slug
												FROM wp_terms
												JOIN wp_term_taxonomy ON wp_term_taxonomy.term_id = wp_terms.term_id
												WHERE taxonomy =  'staff-member-group'
												ORDER BY wp_terms.name");

			if ($mail_envoye != null) {
				include sprintf(
					'%s/../templates/%s_publication_mailenvoye_metabox.php',
					dirname(__FILE__),
					self::POST_TYPE
				);
			} else {
				include sprintf(
					'%s/../templates/%s_publication_mail_metabox.php',
					dirname(__FILE__),
					self::POST_TYPE
				);
			}

			// use reset postdata to restore orginal query
			wp_reset_postdata();
		}

		public function rando_preset_editor_content($content, $post) {
			$defaultContent = get_site_option('nr_rando');

			if ('randonnee' !== $post->post_type) {
				return $content;
			}
			return $defaultContent;
		}

		/**
		 * Save the metaboxes for this custom post type
		 */
		public function save_post($post_id) {
			global $wpdb;

			if ('publish' != get_post_status($post_id)) {
				return;
			}

			// Sauvegarde des paramètres
			$param = array(
				'rando_date',
				'rando_parcours',
				'rando_previsions',
				'rando_responsable_rando',
				'rando_responsable_signaleurs',
				'rando_responsable_cortege',
				'rando_responsable_secouristes',
				'rando_bike1',
				'rando_bike2',
				'rando_driver'
			);

			foreach ($param as &$value) {
				if (isset($_POST[$value])) {
					update_post_meta(
						$post_id,
						$value,
						esc_html($_POST[$value])
					);
				} else {
					delete_post_meta($post_id, $value);
				}
			}

			//Sauvegarde du thumbnail
			$attachment_post_id = get_post_thumbnail_id($post_id);

			/*if($attachment_post_id == null) {
				$attachment_parcours_id = get_post_thumbnail_id($_POST['rando_parcours']);
				set_post_thumbnail($post_ID, $attachment_parcours_id);
			}*/
			$parcours = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT post_title, meta_value
									FROM wp_posts
									LEFT JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.id
									WHERE post_id = %d
									and post_status = 'publish'
									and meta_key = 'parcours_num'",
					$_POST['rando_parcours']
				)
			);

			$pdf_name = '';

			// Récupération info responsable rando
			$resp_rando = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT post_title, meta_value
								FROM wp_posts
								LEFT JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.id
								where id = %d
								and post_status = 'publish'
								and meta_key = '_staff_member_phone'",
					$_POST['rando_responsable_rando']
				)
			);

			// Récupération info responsable signaleurs
			$resp_sign = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT post_title
								FROM wp_posts
								LEFT JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.id
								where id = %d
								and post_status = 'publish'
								and meta_key = '_staff_member_phone'",
					$_POST['rando_responsable_signaleurs']
				)
			);

			// Récupération info responsable staffeurs
			$resp_staf = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT post_title
								FROM wp_posts
								LEFT JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.id
								where id = %d
								and post_status = 'publish'
								and meta_key = '_staff_member_phone'",
					$_POST['rando_responsable_cortege']
				)
			);

			// Récupération info responsable secouristes
			$resp_sec = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT post_title
								FROM wp_posts
								LEFT JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.id
								where id = %d
								and post_status = 'publish'
								and meta_key = '_staff_member_phone'",
					$_POST['rando_responsable_secouristes']
				)
			);

			$biker1 = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT post_title
								FROM wp_posts
								LEFT JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.id
								where id = %d
								and post_status = 'publish'
								and meta_key = '_staff_member_phone'",
					$_POST['rando_bike1']
				)
			);

			$biker2 = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT post_title
								FROM wp_posts
								LEFT JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.id
								where id = %d
								and post_status = 'publish'
								and meta_key = '_staff_member_phone'",
					$_POST['rando_bike2']
				)
			);

			$driver = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT post_title
								FROM wp_posts
								LEFT JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.id
								where id = %d
								and post_status = 'publish'
								and meta_key = '_staff_member_phone'",
					$_POST['rando_driver']
				)
			);

			// Génération du PDF
			if (isset($_POST['rando_declaration_generation'])) {
				$arg = array(
					'point_tan' => esc_html($_POST['rando_point_tan']),
					'rando_date' => $_POST['rando_date_string'],
					'rando_num' => $parcours->meta_value,
					'rando_nom' => $parcours->post_title,
					'rando_previsions' => esc_html($_POST['rando_previsions']),
					'resp_rando_nom' => $resp_rando->post_title,
					'resp_rando_tel' => $resp_rando->meta_value,
					'resp_staf_nom' => $resp_staf->post_title,
					'resp_sign_nom' => $resp_sign->post_title,
					'resp_sec_nom' => $resp_sec->post_title,
					'firstBikerName' => $biker1->post_title,
					'secondBikerName' => $biker2->post_title,
					'rando_driver' => $driver->post_title
				);

				$pdf_name_date = str_replace(
					'/',
					'',
					esc_html($_POST['rando_date'])
				);

				// Génération de la déclaration en pdf AVEC les numéros de tel
				$pdf_name_with_num =
					'declaration_' .
					$pdf_name_date .
					'_parcours' .
					$parcours->meta_value .
					'_' .
					date('YmdHis') .
					'_with_num.pdf';
				//$pdf_with_num = $this->create_pdf($arg, "F", WP_CONTENT_DIR . '/uploads/' . $pdf_name_with_num);
				$pdf_with_num = $this->create_pdf($arg, 'S', '');

				$declaration_pdf_with_num = wp_upload_bits(
					$pdf_name_with_num,
					null,
					$pdf_with_num
				);
				echo $declaration_pdf_with_num[error];
				update_post_meta(
					$post_id,
					'rando_declaration_with_num',
					$declaration_pdf_with_num[url]
				);

				// Génération de la déclaration en pdf SANS les numéros de tel
				$arg['resp_rando_tel'] = '06.  .  .  .  ';
				$arg['resp_staf_tel'] = '06.  .  .  .  ';
				$arg['resp_sign_tel'] = '06.  .  .  .  ';
				$arg['resp_sec_tel'] = '06.  .  .  .  ';
				$pdf_without_num = $this->create_pdf($arg, 'S', '');

				$pdf_name_without_num =
					'declaration_' .
					$pdf_name_date .
					'_parcours' .
					$parcours->meta_value .
					'_' .
					date('YmdHis') .
					'_without_num.pdf';
				$declaration_pdf_without_num = wp_upload_bits(
					$pdf_name_without_num,
					null,
					$pdf_without_num
				);
				update_post_meta(
					$post_id,
					'rando_declaration_without_num',
					$declaration_pdf_without_num[url]
				);

				// Sauvegarde du mail envoyé
				if (isset($_POST['rando_publication_mail'])) {
					$rando_mail = $_POST['rando_mail'];
					$point_tan = $_POST['rando_point_tan'];
					$rando_mail = str_replace(
						'@@POINT_TAN@@',
						$point_tan,
						$rando_mail
					);
					$rando_mail = str_replace(
						'@@DATE@@',
						esc_html($_POST['rando_date']),
						$rando_mail
					);
					$rando_mail = str_replace(
						array("\r\n", "\n"),
						'<br />',
						$rando_mail
					);
					$rando_mail = stripslashes($rando_mail);

					$headers[] = 'Content-Type: text/html; charset=UTF-8';
					$headers[] =
						'From: Arnaud Pioche - Président Nantes-Roller <bureau@nantes-roller.com >' .
						"\r\n";

					$liste_bureau = $wpdb->get_results(
						"SELECT post_title, meta_value
													FROM wp_posts
													JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.id
													JOIN wp_term_relationships ON wp_term_relationships.object_id = wp_posts.id
													JOIN wp_term_taxonomy ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_id
													JOIN wp_terms ON wp_term_taxonomy.term_id = wp_terms.term_id
													WHERE post_status = 'publish'
													AND meta_key = '_staff_member_email'
													AND wp_terms.slug = '" .
							esc_html($_POST['rando_publication_groupe']) .
							"'"
					);

					$subject =
						'Déclaration de randonnée pour le ' .
						$_POST['rando_date_string'];

					$descriptif = @get_post_meta(
						$_POST['rando_parcours'],
						'parcours_descriptif',
						true
					);

					$plan = @get_post_meta(
						$_POST['rando_parcours'],
						'parcours_plan',
						true
					);

					$filename_descriptif = $descriptif['file'];
					$filename_plan = $plan['file'];
					$filename_declaration = $declaration_pdf_with_num['file'];

					$attachments = array();
					array_push($attachments, $filename_descriptif);
					array_push($attachments, $filename_plan);
					array_push($attachments, $filename_declaration);

					foreach ($liste_bureau as $membre) {
						wp_mail(
							$membre->meta_value,
							$subject,
							$rando_mail,
							$headers,
							$attachments
						);
					}

					update_post_meta($post_id, 'rando_mail', $rando_mail);
				}
			}

			$parcours_distance = $wpdb->get_row(
				$wpdb->prepare(
					"SELECT post_title, meta_value
								FROM wp_posts
								LEFT JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.id
								WHERE post_id = %d
								and post_status = 'publish'
								and meta_key = 'parcours_distance'",
					$_POST['rando_parcours']
				)
			);

			//$subject = $_POST['rando_date_string']." : ".$parcours->post_title;
			$post = get_post($post_id);
			$subject = $post->post_title;

			// Préparation du message
			$user_post = $_POST['rando_forum_user'];
			$sDescriptionForum = get_site_option('nr_forum');
			$sDescriptionForum = str_replace(
				'#DATE_FR#',
				$_POST['rando_date_string'],
				$sDescriptionForum
			);
			$sDescriptionForum = str_replace(
				'#DISTANCE_PARCOURS#',
				$parcours_distance->meta_value,
				$sDescriptionForum
			);

			$url_thumbnail_post = get_the_post_thumbnail_url(
				get_post()->ID,
				'medium'
			);

			if (!isset($url_thumbnail_post) || $url_thumbnail_post == '') {
				$url_thumbnail_post = get_the_post_thumbnail_url(
					@get_post_meta(get_post()->ID, 'rando_parcours', true),
					'medium'
				);
			}

			$sDescriptionForum = str_replace(
				'#PARCOURS_IMAGE#',
				$url_thumbnail_post,
				$sDescriptionForum
			);
			$sDescriptionForum = str_replace(
				'#LIEN_PARCOURS_NUM#',
				get_permalink($_POST['rando_parcours']),
				$sDescriptionForum
			);

			$sDescriptionForum = str_replace(
				'#RESP_RANDO#',
				$resp_rando->post_title,
				$sDescriptionForum
			);
			$sDescriptionForum = str_replace(
				'#RESP_STAF#',
				$resp_staf->post_title,
				$sDescriptionForum
			);
			$sDescriptionForum = str_replace(
				'#RESP_SIGN#',
				$resp_sign->post_title,
				$sDescriptionForum
			);
			$sDescriptionForum = str_replace(
				'#RESP_SEC#',
				$resp_sec->post_title,
				$sDescriptionForum
			);
			$sDescriptionForum = str_replace(
				'#BIKE1#',
				$biker1->post_title,
				$sDescriptionForum
			);
			$sDescriptionForum = str_replace(
				'#BIKE2#',
				$biker2->post_title,
				$sDescriptionForum
			);
			$sDescriptionForum = str_replace(
				'#DRIVER#',
				$driver->post_title,
				$sDescriptionForum
			);

			$sDescriptionForum = str_replace(
				'#OBSERVATIONS#',
				esc_html(strip_tags($_POST['rando_point_tan'])),
				$sDescriptionForum
			);

			$sDescriptionForum = str_replace(
				'#MESSAGE_FIN#',
				esc_html($_POST['rando_forum_message_fin']),
				$sDescriptionForum
			);

			$pdf = @get_post_meta(
				$post_id,
				'rando_declaration_without_num',
				true
			);

			update_post_meta($post_id, 'forumContent', $sDescriptionForum);
		}

		public function nr_set_html_mail_content_type() {
			return 'text/html';
		}

		private function create_pdf($arg, $output, $output_name) {
			$img_dir = ABSPATH . 'wp-content/plugins/nantes-roller/img/';

			$date = $arg['rando_date'];
			$rando_num = $arg['rando_num'];
			$rando_nom = $arg['rando_nom'];
			$prevision = $arg['rando_previsions'];
			$resp_rando_nom = $arg['resp_rando_nom'];
			$resp_rando_tel = $arg['resp_rando_tel'];
			$resp_staf_nom = $arg['resp_staf_nom'];
			$resp_sign_nom = $arg['resp_sign_nom'];
			$resp_sec_nom = $arg['resp_sec_nom'];
			$firstBikerName = $arg['firstBikerName'];
			$secondBikerName = $arg['secondBikerName'];
			$rando_driver = $arg['rando_driver'];
			$observations = utf8_decode(
				strip_tags(html_entity_decode($arg['point_tan']))
			);
			$date_signature = date('d/m/Y');

			$pdf = new FPDF();
			$pdf->SetMargins(20, 15);
			$pdf->AddPage();
			$pdf->SetFont('Arial', 'B', 20);
			$pdf->Image($img_dir . 'nr_logo.png', 20, 15, 35, 18);
			$pdf->SetX(55);
			$pdf->MultiCell(
				100,
				8,
				utf8_decode(
					'Fiche de déclaration hebdomadaire de la randonnée roller du jeudi soir'
				),
				0,
				'C',
				0
			);
			$pdf->Image($img_dir . 'nantes.png', 155, 15);
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->MultiCell(
				170,
				8,
				utf8_decode(
					'À remettre le jeudi précédent ladite randonnée au Chef de Police Municipale'
				),
				0,
				'C',
				0
			);
			$pdf->SetFont('Arial', '', 11);

			$nWidthString = $pdf->GetStringWidth(
				utf8_decode('Jeudi concerné : ')
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Jeudi concerné : '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode($date),
				'RT',
				1,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 0);

			$nWidthString = $pdf->GetStringWidth(
				utf8_decode('Itinéraire choisi : ')
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Itinéraire choisi : '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				140 - $nWidthString,
				8,
				utf8_decode($rando_nom),
				'T',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 0);
			$nWidthString = $pdf->GetStringWidth(utf8_decode('Parcours n° '));
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Parcours n° '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$nX = $pdf->GetX() - 20;
			$pdf->Cell(170 - $nX, 8, $rando_num, 'RT', 1, 'L', false);
			$pdf->SetTextColor(0, 0, 0);

			$nWidthString = $pdf->GetStringWidth(
				utf8_decode('Nombre de participants attendus : ')
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Nombre de participants attendus : '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_encode($prevision),
				'RT',
				1,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 0);

			$nWidthString = $pdf->GetStringWidth(
				utf8_decode('Heure de départ : ')
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Heure de départ : '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode('20h45'),
				'RT',
				1,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 0);

			$nWidthString = $pdf->GetStringWidth(
				utf8_decode("Heure d'arrivée : ")
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode("Heure d'arrivée : "),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode('22h45'),
				'RT',
				1,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 0);

			$nWidthString = $pdf->GetStringWidth(
				utf8_decode('Coordinateur·rice de la randonnée : ')
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Coordinateur·rice de la randonnée : '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode($resp_rando_nom),
				'RT',
				1,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 0);

			$nWidthString = $pdf->GetStringWidth(
				utf8_decode('N° de téléphone portable : ')
			);
			$pdf->Cell(
				$nWidthString,
				5,
				utf8_decode('N° de téléphone portable : '),
				'L',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);

			$pdf->Cell(
				170 - $nWidthString,
				5,
				$resp_rando_tel,
				'R',
				1,
				'L',
				false
			);
			$pdf->SetTextColor(255, 255, 255);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Cell(
				170,
				8,
				utf8_decode("Organisation du service d'ordre et de secours"),
				'LR',
				1,
				'C',
				true
			);
			$pdf->SetTextColor(0, 0, 0);

			$pdf->SetFillColor(125);
			$pdf->Cell(
				170,
				8,
				utf8_decode('Nom des responsables'),
				'L',
				1,
				'C',
				true
			);
			$pdf->SetFillColor(0);
			$pdf->SetFont('Arial', '', 11);
			$nWidthString = $pdf->GetStringWidth(
				utf8_decode('Responsable du cortège : ')
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Responsable du cortège : '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode($resp_staf_nom),
				'TR',
				1,
				'L',
				false
			);

			$pdf->SetTextColor(0, 0, 0);

			$nWidthString = $pdf->GetStringWidth(
				utf8_decode('Responsable des signaleur·se·s : ')
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Responsable des signaleur·se·s : '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode($resp_sign_nom),
				'TR',
				1,
				'L',
				false
			);

			$pdf->SetTextColor(0, 0, 0);
			$nWidthString = $pdf->GetStringWidth(
				utf8_decode('Responsable des secouristes : ')
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Responsable des secouristes : '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode($resp_sec_nom),
				'TR',
				1,
				'L',
				false
			);

			define('FIRST_BIKE_TITLE', 'Cycliste n°1 : ');
			$pdf->SetTextColor(0, 0, 0);
			$nWidthString = $pdf->GetStringWidth(utf8_decode(FIRST_BIKE_TITLE));
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode(FIRST_BIKE_TITLE),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode($firstBikerName),
				'TR',
				1,
				'L',
				false
			);

			define('SECOND_BIKE_TITLE', 'Cycliste n°2 : ');
			$pdf->SetTextColor(0, 0, 0);
			$nWidthString = $pdf->GetStringWidth(
				utf8_decode(SECOND_BIKE_TITLE)
			);
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode(SECOND_BIKE_TITLE),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode($secondBikerName),
				'TR',
				1,
				'L',
				false
			);

			define('DRIVER_TITLE', 'Conducteur·rice du véhicule balai : ');
			$pdf->SetTextColor(0, 0, 0);
			$nWidthString = $pdf->GetStringWidth(utf8_decode(DRIVER_TITLE));
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode(DRIVER_TITLE),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				170 - $nWidthString,
				8,
				utf8_decode($rando_driver),
				'TR',
				1,
				'L',
				false
			);

			$pdf->SetTextColor(255, 255, 255);

			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Cell(
				170,
				8,
				utf8_decode('Relevé des effectifs'),
				'LR',
				1,
				'C',
				true
			);
			$pdf->SetTextColor(255, 0, 0);
			$pdf->SetFont('Arial', '', 11);
			$pdf->Cell(
				170,
				8,
				utf8_decode("(à renseigner à l'issue du Tour de Chauffe)"),
				'LR',
				1,
				'C',
				true
			);
			$pdf->SetTextColor(0, 0, 0);

			$pdf->Cell(
				170,
				8,
				utf8_decode('Nombre de participants :'),
				'LR',
				1,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 0);

			$pdf->Cell(
				170,
				8,
				utf8_decode('Nombre de signaleurs :'),
				'LTR',
				1,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 0);

			$pdf->SetFont('Arial', 'B', 11);

			$pdf->Cell(
				170,
				8,
				utf8_decode('Observations éventuelles'),
				'LTR',
				1,
				'L',
				false
			);
			$pdf->SetFont('Arial', '', 11);

			$aStringSearch = array('&rsquo;');
			$aStringReplace = array("'");
			$pdf->MultiCell(
				170,
				4,
				str_replace(
					$aStringSearch,
					$aStringReplace,
					html_entity_decode(strip_tags($observations))
				),
				'LR',
				'L',
				false
			);
			$nWidthString = $pdf->GetStringWidth(utf8_decode('Nantes, le '));
			$pdf->Cell(
				$nWidthString,
				8,
				utf8_decode('Nantes, le '),
				'LT',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 255);
			$pdf->Cell(
				85 - $nWidthString,
				8,
				utf8_decode($date_signature),
				'TR',
				0,
				'L',
				false
			);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetFont('Arial', 'B', 11);
			$pdf->Cell(
				85,
				8,
				utf8_decode('Visa Chef de Police Municipale :'),
				'TR',
				1,
				'C',
				false
			);

			$pdf->Cell(
				85,
				8,
				utf8_decode("Pour l'association Nantes-Roller : "),
				'LR',
				0,
				'C',
				false
			);
			$pdf->Cell(85, 8, utf8_decode(''), 'R', 1, 'L', false);

			$pdf->Cell(85, 10, utf8_decode(''), 'LBR', 0, 'L', false);
			$pdf->Cell(85, 10, utf8_decode(''), 'RB', 1, 'L', false);

			return $pdf->Output($output_name, $output);
		}
	}
}
