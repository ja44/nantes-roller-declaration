<?php

date_default_timezone_set('Europe/Paris');
define('NR_FORUM_USERS_TABLE', 'phpbb_users');
define('NR_FORUM_TOPICS_TABLE', 'phpbb_topics');
define('NR_FORUM_POSTS_TABLE', 'phpbb_posts');
define('NR_FORUM', 'http://www.nantes-roller.com/forum/');
define('FORUM_ROOT_PATH', '../../../../../forum/');
define('URL_UPLOADS', 'http://www.nantes-roller.com/wp-content/uploads/');
define('ID_DECLARATION', 'declaration_rando');
define('ID_RESP_RANDO', 'responsable-randonnee');
define('ID_RESP_SIGN', 'responsable-signaleurs');
define('ID_RESP_STAFF', 'responsable-staffeurs');
define('ID_RESP_SEC', 'responsable-secouristes');
?>
