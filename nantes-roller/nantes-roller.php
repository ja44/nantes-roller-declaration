<?php /*
Plugin Name: Nantes-Roller
Description: Wordpress plugin to declare the street skatings.
Author: Nantes-Roller
Author URI: http://www.nantes-roller.com
Version: 1.0
License: MIT
*/

include_once plugin_dir_path(__FILE__) . '/settings.php';
include_once plugin_dir_path(__FILE__) . '/post-types/post_type_parcours.php';
include_once plugin_dir_path(__FILE__) .
	'/post-types/class-nantes-roller-admin.php';
include_once plugin_dir_path(__FILE__) . '/forumwidget.php';

if (!class_exists('WP_NR_Plugin')) {
	class WP_NR_Plugin {
		/**
		 * Construct the plugin object
		 */
		public function __construct() {
			$PostTypeParcours = new PostTypeParcours();
			$PostTypeRando = new PostTypeRando();
			$nantesRollerAdmin = new NantesRollerAdmin();
			add_action('widgets_init', array($this, 'widget_init'));
		}

		public function widget_init() {
			register_widget('NR_Forum_Widget');
		}

		/**
		 * hook into WP's admin_init action hook
		 */
		public function admin_init() {
			// Set up the settings for this plugin
			$this->init_settings();
			// Possibly do additional admin_init tasks
		}

		/**
		 * add a menu
		 */
		public function add_menu() {
			add_options_page(
				'Parcours',
				'Parcours',
				'manage_options',
				'WP_NR_Plugin',
				array(&$this, 'plugin_settings_page')
			);
		}

		/**
		 * Menu Callback
		 */
		public function plugin_settings_page() {
			if (!current_user_can('manage_options')) {
				wp_die(
					__(
						'You do not have sufficient permissions to access this page.'
					)
				);
			}

			// Render the settings template
			include sprintf('%s/templates/settings.php', dirname(__FILE__));
		}
	}

	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('WP_NR_Plugin', 'activate'));
	register_deactivation_hook(__FILE__, array('WP_NR_Plugin', 'deactivate'));

	// instantiate the plugin class
	$WP_NR_Plugin = new WP_NR_Plugin();

	// Add a link to the settings page onto the plugin page
	if (isset($wp_nr_plugin)) {
		// Add the settings link to the plugins page
		function plugin_settings_link($links) {
			$settings_link =
				'<a href="options-general.php?page=plugin_settings_page">Settings</a>';
			array_unshift($links, $settings_link);
			return $links;
		}

		$plugin = plugin_basename(__FILE__);
		add_filter("plugin_action_links_$plugin", 'plugin_settings_link');
	}
}
