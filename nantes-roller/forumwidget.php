<?php
class NR_Forum_Widget extends WP_Widget {
	public function __construct() {
		parent::__construct('nr_forum_widget', 'Forum', array(
			'description' => 'Les derniers messages du forum.'
		));
		add_action('wp_ajax_getForumMessages', array(
			$this,
			'getForumMessages'
		));
		add_action('wp_ajax_nopriv_getForumMessages', array(
			$this,
			'getForumMessages'
		));
	}

	function add_js_scripts() {
		//wp_enqueue_script( 'script', get_template_directory_uri().'/js/script.js', array('jquery'), '1.0', true );

		// pass Ajax Url to script.js
		wp_localize_script('script', 'ajaxurl', admin_url('admin-ajax.php'));
	}

	public function widget($args, $instance) {
		echo $args['before_widget'];

		if (!empty($instance['title'])) {
			echo '<div class="widget-title-container"><h1 class="widget-title nr_forum_title">' .
				apply_filters('widget_title', $instance['title']) .
				'</h1></div>';
		}

		echo "<div id='nr_forum'></div>";

		echo "<script type='text/javascript'>

			setInterval(function() {
				refresh_forum();
			}, 600000);

			refresh_forum();

			jQuery(document).ready(function(){

			  jQuery('.nr_forum_title').click(function(){
				refresh_forum();
			  }
			  );
			});

			function refresh_forum() {
				var ajaxurl = '" .
			admin_url('admin-ajax.php') .
			"';
				document.getElementById('nr_forum').innerHTML = '<img src=\'http://nantes-roller.com/wp-content/plugins/nantes-roller/img/loading.gif\' />';
				jQuery.post(
						ajaxurl,
						{
							'action': 'getForumMessages'
						},
						function(response){
							document.getElementById('nr_forum').innerHTML = response;
						}
					);
				}

		</script>";

		echo $args['after_widget'];
	}

	public function form($instance) {
		$title = isset($instance['title']) ? $instance['title'] : ''; ?>
		<p>
			<label for="<?php echo $this->get_field_name(
   	'title'
   ); ?>"><?php _e('Title:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id(
   	'title'
   ); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<?php
	}

	private function getHtmlMessages() {
		global $wpdb;

		$sql =
			'SELECT post_id, poster_id, post_subject, post_time FROM ' .
			NR_FORUM_POSTS_TABLE .
			' order by post_time desc limit 10';
		$results = $wpdb->get_results($sql);

		$abbreviatedMonthNames = array(
			'01',
			'02',
			'03',
			'04',
			'05',
			'06',
			'07',
			'08',
			'09',
			'10',
			'11',
			'12'
		);
		$messages = '';
		if ($results) {
			$messages = '<ul>';
			foreach ($results as $post) {
				$user = $wpdb->get_row(
					'SELECT username FROM ' .
						NR_FORUM_USERS_TABLE .
						" WHERE user_id = $post->poster_id"
				);
				$dateDb = date('d/m/Y', $post->post_time);
				$dateServeur = date('d/m/Y');
				$date =
					date('d', $post->post_time) .
					'/' .
					$abbreviatedMonthNames[date('n', $post->post_time) - 1] .
					' ' .
					date('H:i', $post->post_time);
				if ($dateDb == $dateServeur) {
					$bold = 'bold';
				} else {
					$bold = 'normal';
				}

				$url =
					NR_FORUM .
					'viewtopic.php?p=' .
					$post->post_id .
					'#p' .
					$post->post_id;
				$messages .=
					"<li><a class='moretag' target='_blank' href='" .
					$url .
					"' title='" .
					$post->post_subject .
					"'>" .
					$post->post_subject .
					"</a><br /><font style='font-size:12px; color:#AAAAAA; font-weight:" .
					$bold .
					"'>" .
					$date .
					"</font><font style='font-size:12px; color:#AAAAAA;'> | " .
					$user->username .
					'</font></li>';
			}
			$messages .= '</ul>';
			$messages .=
				"<span style='font-size: 10px;font-style: italic;'>maj à " .
				date('H:i:s') .
				'</span>';
		}

		return $messages;
	}

	public function getForumMessages() {
		echo $this->getHtmlMessages();

		die();
	}
}
