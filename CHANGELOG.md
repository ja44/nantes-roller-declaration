# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## Breaking changes

- news groups to add : cyclistes, conducteurs

### Added

- rename group for the bikers
- rename field form
- rename field title
- new fields "bike1, bike2" on the format
- add 2 rows on the pdf to show the bike1 and bike2
- remove phone number
- add driver field
